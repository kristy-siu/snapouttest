# Snap Out Test

Return best journey suggestions based on start, end, and departure time

[Deployed App](http://snapouttest-env-2.rub2k76v4n.us-west-2.elasticbeanstalk.com/)

##### Example curl (using provided request json file)

``` curl -vX POST http://snapouttest-env-2.rub2k76v4n.us-west-2.elasticbeanstalk.com/suggest  -d @request.json --header "Content-Type: application/json" ```

# API

### Get API Version
``` GET /```

#### Request

```None```

#### Response

```Best Route Suggestion API v1.0```


### Get suggestions
``` POST /suggest```

#### Request

``` 
{
	"start": string # start location,
	"end": string # destination,
	"time": string # date or time,
	"options": { # Optional
		"prioritize": {
			"least_walking": int # priority 1-5,
			"shortest": int # priority 1-5,
			"green_friendly": int # priority 1-5,
			"least_traffic": int # priority 1-5,
			"quickest": int # priority 1-5
		},
		"excluded_transport_modes": list # walking | bicycling | driving | transit,
		"restrictions": {
            "distance": {
                "minimum_drive": float # (km),
                "maximum_walk": float # (km)
            },
            "time": {
                "minimum_drive": int # (minutes),
                "maximum_walk": float # (minutes)
            }
        }
		
    }
}		
```

#### Response

```
{ 
	"journeys": list # list of journeys with an integer ranking of suitability where more is better,
	"error": string # error message or empty string
}
```
**Exceptions**

``` NoRoutesFound ```
Thrown when there are no valid routes found.

``` InvalidDateTime ```
Thrown when the journey start time cannot be resolved to a valid date/time.

Configuration
-
#### Options
>```prioritize```

Expects a JSON object with the following weighted priorities from 1 (low) to 5 (high):
- least_walking
- shortest
- green_friendly
- least_traffic
- quickest

**Unspecified priorities will default to 1**

**Exceptions**

``` UnknownPriorityKey ```
Thrown when the object received has an unrecognised key value.

``` PriorityOutOfRange ```
Thrown when the object received has a priority value outside of the range 1-5.

``` InvalidPriorityValue ```
Thrown when the object received has a priority value which cannot be parsed to an integer.


>``` excluded_transport_modes```

Expects a list of modes to exclude from the following values:
- walking
- cycling
- driving
- transit

**Exceptions**

``` UnknownTransportMode ```
Thrown when the list contains an unknown mode value e.g. flying.

>``` restrictions```

Expects a JSON object with one or more of the  following possible restrictions
- minimum_drive_distance
Driving journeys with total distance less than the provided value in kilometres should be excluded.
- maximum_walk_distance
Journeys should not include a single walk of this distance or more.
- minimum_drive_time
Driving journeys with travel time of less than the provided value in minutes should be excluded.
- maximum_walk_time
Journeys should not include a single walk of which will last greater than this.

**Exceptions**

``` UnknownRestrictionKey ```
Thrown when the object received has an unrecognised key value.

``` InvalidRestrictionValue ```
Thrown when any of the keys has a value which cannot be parsed to an integer (time) or float (distance).

Assumptions
-

- The google maps api returns results in the expected format
- Direct journeys **only** are considered
- The API client can process journey data in the format returned natively from google maps API (JSON)

Improvements
-

- Add via waypoints
- Allow configuration of each transport mode
- Allow begin to be a range to retrieve journeys incrementally through the time range allowing a best time to depart 
suggestion
- Better validation
- System to allow previous user feedback to modify results e.g. weight routes according to previously recorded
preferences
- More granular testing
- Specified response formats e.g. XML
- More test fixtures
- Test fixtures in separate file
- Gracefully handling of exceptions thrown by the google maps API