import unittest

import app

# <editor-fold desc="CONSTANTS">

GREETING = 'Best Route Suggestion API v1.0'

TEST_JOURNEYS = [
    {
        'distance': 1000,
        'journey': '',
        'mode': 'walking',
        'duration': 20,
        'walking_distance': 1000,
        'driving_time': 0,
        'time_in_traffic': 0,
        'walking_time': 20,
        'driving_distance': 0
    },
    {
        'distance': 900,
        'journey': '',
        'mode': 'bicycling',
        'duration': 10,
        'walking_distance': 0,
        'driving_time': 0,
        'time_in_traffic': 0,
        'walking_time': 0,
        'driving_distance': 0
    },
    {
        'distance': 950,
        'journey': '',
        'mode': 'transit',
        'duration': 5,
        'walking_distance': 100,
        'driving_time': 0,
        'time_in_traffic': 0,
        'walking_time': 0,
        'driving_distance': 0
    },
    {
        'distance': 975,
        'journey': '',
        'mode': 'driving',
        'duration': 5,
        'walking_distance': 100,
        'driving_time': 2,
        'time_in_traffic': 1,
        'walking_time': 0,
        'driving_distance': 975
    }
]
CORRECT_RANKING = [
    {'distance': 1000,
     'driving_distance': 0,
     'driving_time': 0,
     'duration': 20,
     'journey': '',
     'mode': 'walking',
     'ranks': {'green_friendly': 3,
               'least_traffic': 3,
               'least_walking': 0,
               'quickest': 0,
               'shortest': 0},
     'time_in_traffic': 0,
     'walking_distance': 1000,
     'walking_time': 20},
    {'distance': 900,
     'driving_distance': 0,
     'driving_time': 0,
     'duration': 10,
     'journey': '',
     'mode': 'bicycling',
     'ranks': {'green_friendly': 2,
               'least_traffic': 2,
               'least_walking': 3,
               'quickest': 1,
               'shortest': 3},
     'time_in_traffic': 0,
     'walking_distance': 0,
     'walking_time': 0},
    {'distance': 950,
     'driving_distance': 0,
     'driving_time': 0,
     'duration': 5,
     'journey': '',
     'mode': 'transit',
     'ranks': {'green_friendly': 1,
               'least_traffic': 1,
               'least_walking': 2,
               'quickest': 3,
               'shortest': 2},
     'time_in_traffic': 0,
     'walking_distance': 100,
     'walking_time': 0},
    {'distance': 975,
     'driving_distance': 975,
     'driving_time': 2,
     'duration': 5,
     'journey': '',
     'mode': 'driving',
     'ranks': {'green_friendly': 0,
               'least_traffic': 0,
               'least_walking': 1,
               'quickest': 2,
               'shortest': 1},
     'time_in_traffic': 1,
     'walking_distance': 100,
     'walking_time': 0}]
CORRECT_PRIORITIZED_RANKING = [{'distance': 1000, 'driving_distance': 0,
                                'ranks': {'least_traffic': 12, 'least_walking': 0, 'quickest': 0, 'shortest': 0,
                                          'green_friendly': 9}, 'walking_distance': 1000, 'driving_time': 0,
                                'walking_time': 20, 'journey': '', 'mode': 'walking', 'duration': 20,
                                'time_in_traffic': 0}, {'distance': 900, 'driving_distance': 0,
                                                        'ranks': {'least_traffic': 8, 'least_walking': 3, 'quickest': 5,
                                                                  'shortest': 6, 'green_friendly': 6},
                                                        'walking_distance': 0, 'driving_time': 0, 'walking_time': 0,
                                                        'journey': '', 'mode': 'bicycling', 'duration': 10,
                                                        'time_in_traffic': 0}, {'distance': 950, 'driving_distance': 0,
                                                                                'ranks': {'least_traffic': 4,
                                                                                          'least_walking': 2,
                                                                                          'quickest': 15, 'shortest': 4,
                                                                                          'green_friendly': 3},
                                                                                'walking_distance': 100,
                                                                                'driving_time': 0, 'walking_time': 0,
                                                                                'journey': '', 'mode': 'transit',
                                                                                'duration': 5, 'time_in_traffic': 0},
                               {'distance': 975, 'driving_distance': 975,
                                'ranks': {'least_traffic': 0, 'least_walking': 1, 'quickest': 10, 'shortest': 2,
                                          'green_friendly': 0}, 'walking_distance': 100, 'driving_time': 2,
                                'walking_time': 0, 'journey': '', 'mode': 'driving', 'duration': 5,
                                'time_in_traffic': 1}]

FULL_CORRECT_RANKING = [{'distance': 1000, 'driving_distance': 0,
                         'ranks': {'least_traffic': 3, 'least_walking': 0, 'quickest': 0, 'shortest': 0,
                                   'green_friendly': 3}, 'walking_distance': 1000, 'driving_time': 0,
                         'total_ranking': 6, 'walking_time': 20, 'journey': '', 'mode': 'walking', 'duration': 20,
                         'time_in_traffic': 0}, {'distance': 900, 'driving_distance': 0,
                                                 'ranks': {'least_traffic': 2, 'least_walking': 3, 'quickest': 1,
                                                           'shortest': 3, 'green_friendly': 2}, 'walking_distance': 0,
                                                 'driving_time': 0, 'total_ranking': 11, 'walking_time': 0,
                                                 'journey': '', 'mode': 'bicycling', 'duration': 10,
                                                 'time_in_traffic': 0}, {'distance': 950, 'driving_distance': 0,
                                                                         'ranks': {'least_traffic': 1,
                                                                                   'least_walking': 2, 'quickest': 3,
                                                                                   'shortest': 2, 'green_friendly': 1},
                                                                         'walking_distance': 100, 'driving_time': 0,
                                                                         'total_ranking': 9, 'walking_time': 0,
                                                                         'journey': '', 'mode': 'transit',
                                                                         'duration': 5, 'time_in_traffic': 0},
                        {'distance': 975, 'driving_distance': 975,
                         'ranks': {'least_traffic': 0, 'least_walking': 1, 'quickest': 2, 'shortest': 1,
                                   'green_friendly': 0}, 'walking_distance': 100, 'driving_time': 2, 'total_ranking': 4,
                         'walking_time': 0, 'journey': '', 'mode': 'driving', 'duration': 5, 'time_in_traffic': 1}]


TEST_DATA = {
    "start": "CT2 8AG",
    "end": "ME5 4AB",
    "begin": "11.30pm",
    "options": {
        "prioritize": {
            "least_walking": 1,
            "shortest": 2,
            "green_friendly": 1,
            "least_traffic": 4,
            "quickest": 5
        },
        "excluded_transport_modes": ['transit'],
        "restrictions": {
            "distance": {
                "minimum_drive": 1.5,
                "maximum_walk": 2
            },
            "time": {
                "minimum_drive": 5,
                "maximum_walk": 60
            }
        }
    }
}
TEST_DATA2 = {
    "start": "CT2 8AG",
    "end": "ME5 4AB",
    "begin": "blue",
    "options": {
        "prioritize": {
            "least_walking": 1,
            "shortest": 2,
            "green_friendly": 1,
            "least_traffic": 4,
            "quickest": 5
        },
        "excluded_transport_modes": ['transit'],
        "restrictions": {
            "distance": {
                "minimum_drive": 1.5,
                "maximum_walk": 2
            },
            "time": {
                "minimum_drive": 5,
                "maximum_walk": 60
            }
        }
    }
}
# Correct
OPTIONS1 = {  # Optional
    "prioritize": {
        "least_walking": 1,  # priority 1-5
        "shortest": 2,  # priority 1-5
        "green_friendly": 3,  # priority 1-5
        "least_traffic": 4,  # priority 1-5
        "quickest": 5,  # priority 1-5
    },
    "via": [],  # waypoints (string),
    "excluded_transport_modes": [],  # walking | bicycling | driving | public_transport,
    "restrictions": {
        "distance": {
            "minimum_drive": 1.5,  # (km),
            "maximum_walk": 500  # (km),
        },
        "time": {
            "minimum_drive": 15,  # (minutes),
            "maximum_walk": 10000  # (minutes)
        }
    }
}

# Out of range priority
OPTIONS2 = {  # Optional
    "prioritize": {
        "least_walking": 99,  # priority 1-5
        "shortest": 2,  # priority 1-5
        "green_friendly": 3,  # priority 1-5
        "least_traffic": 4,  # priority 1-5
        "quickest": 5,  # priority 1-5
    },
    "via": [],  # waypoints (string),
    "excluded_transport_modes": [],  # walking | bicycling | driving | public_transport,
    "restrictions": {
        "distance": {
            "minimum_drive": 1.5,  # (km),
            "maximum_walk": 0.5  # (km),
        },
        "time": {
            "minimum_drive": 15,  # (minutes),
            "maximum_walk": 10  # (minutes)
        }
    }
}

# Invalid priority
OPTIONS3 = {  # Optional
    "prioritize": {
        "least_walking": "hello world",  # priority 1-5
        "shortest": 2,  # priority 1-5
        "green_friendly": 3,  # priority 1-5
        "least_traffic": 4,  # priority 1-5
        "quickest": 5,  # priority 1-5
    },
    "via": [],  # waypoints (string),
    "excluded_transport_modes": [],  # walking | bicycling | driving | public_transport,
    "restrictions": {
        "distance": {
            "minimum_drive": 1.5,  # (km),
            "maximum_walk": 0.5  # (km),
        },
        "time": {
            "minimum_drive": 15,  # (minutes),
            "maximum_walk": 10  # (minutes)
        }
    }
}

# Invalid transport mode
OPTIONS4 = {  # Optional
    "prioritize": {
        "least_walking": 1,  # priority 1-5
        "shortest": 2,  # priority 1-5
        "green_friendly": 3,  # priority 1-5
        "least_traffic": 4,  # priority 1-5
        "quickest": 5,  # priority 1-5
    },
    "via": [],  # waypoints (string),
    "excluded_transport_modes": ["flying"],  # walking | bicycling | driving | public_transport,
    "restrictions": {
        "distance": {
            "minimum_drive": 1.5,  # (km),
            "maximum_walk": 0.5  # (km),
        },
        "time": {
            "minimum_drive": 15,  # (minutes),
            "maximum_walk": 10  # (minutes)
        }
    }
}

# Invalid restriction key
OPTIONS5 = {  # Optional
    "prioritize": {
        "least_walking": 1,  # priority 1-5
        "shortest": 2,  # priority 1-5
        "green_friendly": 3,  # priority 1-5
        "least_traffic": 4,  # priority 1-5
        "quickest": 5,  # priority 1-5
    },
    "excluded_transport_modes": [],  # walking | bicycling | driving | public_transport,
    "restrictions": {
        "distance": {
            "minimum_fly": 1.5,  # (km),
            "maximum_walk": 0.5  # (km),
        },
        "time": {
            "minimum_drive": 15,  # (minutes),
            "maximum_walk": 10  # (minutes)
        }
    }
}


# </editor-fold>


class TestSuggestion(unittest.TestCase):

    def test_get_journeys(self):
        journeys = app._get_journeys(TEST_DATA["start"], TEST_DATA["end"], TEST_DATA["begin"], options=OPTIONS1)

        with self.assertRaises(app.InvalidDateTimeValue):
            app._get_journeys(TEST_DATA2["start"], TEST_DATA2["end"], TEST_DATA2["begin"], options=OPTIONS1)


    def test_ranking(self):
        self.maxDiff = None
        journeys = app._rank_journeys(TEST_JOURNEYS)
        self.assertItemsEqual(journeys, CORRECT_RANKING)

    def test_prioritize(self):
        journeys = app._prioritize(CORRECT_RANKING, OPTIONS1)
        self.assertItemsEqual(journeys, CORRECT_PRIORITIZED_RANKING)

    def test_calculate_totals(self):
        journeys = app._calculate_total(CORRECT_RANKING)
        self.assertItemsEqual(journeys, FULL_CORRECT_RANKING)

    def test_validate_options(self):
        self.assertTrue(app._validate_options(OPTIONS1))
        with self.assertRaises(app.OutOfRangePriorityValue):
            app._validate_options(OPTIONS2)
        with self.assertRaises(app.InvalidPriorityValue):
            app._validate_options(OPTIONS3)
        with self.assertRaises(app.UnknownTransportMode):
            app._validate_options(OPTIONS4)
        with self.assertRaises(app.UnknownRestrictionKey):
            app._validate_options(OPTIONS5)

    def test_get_suggestions(self):
        app._get_suggestions(TEST_DATA)
        self.assertTrue(True)


class TestApiVersion(unittest.TestCase):

    def test_greeting(self):
        self.assertEqual(app.api(), GREETING)


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestApiVersion)
    unittest.TextTestRunner(verbosity=2).run(suite)
    suite = unittest.TestLoader().loadTestsFromTestCase(TestSuggestion)
    unittest.TextTestRunner(verbosity=2).run(suite)
