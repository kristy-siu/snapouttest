""" Main app """

from flask import Flask, request
from googlemaps import directions, client
from datetime import datetime
from dateutil import parser

import json, copy

app = Flask(__name__)

DIRECTIONS_API_KEY = "AIzaSyCpwpSikyqVEiOofTDMQAUbiFqrUcDlYBo"

# Valid Keys
PRIORITY_KEYS = [
    'least_walking',
    'shortest',
    'green_friendly',
    'least_traffic',
    'quickest'
]

RESTRICTION_KEYS = [
    'distance',
    'time',
    'maximum_walk',
    'minimum_drive'
]

TRANSPORT_MODES = [
    'walking',
    'bicycling',
    'transit',
    'driving'
]


@app.route('/')
def api():
    return 'Best Route Suggestion API v1.0'


@app.route('/suggest', methods=['POST'])
def suggest():
    post_data = request.get_json()
    return _get_suggestions(post_data)


def _get_suggestions(data):
    options = None
    if 'options' in data.keys():
        options = data['options']
        try:
            _validate_options(options)
        except Exception as e:
            return json.dumps({
                "journeys": [],
                "error": e.message
            })
    try:
        journeys = _get_journeys(data['start'], data['end'], data['begin'], data['options'])
    except Exception as e:
        return json.dumps({
            "journeys": [],
            "error": e.message
        })
    journeys = _rank_journeys(journeys)
    if options:
        journeys = _prioritize(journeys, options)
    journeys = _calculate_total(journeys)

    return json.dumps(journeys)


# <editor-fold desc="VALIDATION">

# Validate options
def _validate_options(options):
    if "prioritize" in options.keys():
        _validate_prioritize(options["prioritize"])
    if "excluded_transport_modes" in options.keys():
        _validate_exclusions(options["excluded_transport_modes"])
    if "restrictions" in options.keys():
        _validate_restrictions(options["restrictions"])
    return True


def _validate_prioritize(prioritize):
    invalid = filter(lambda x: True if x not in PRIORITY_KEYS else False, prioritize.keys())

    if len(invalid) > 0:
        raise UnknownPriorityKey(invalid)

    invalid = filter(lambda x: not _check_integer(x), prioritize.values())

    if len(invalid) > 0:
        raise InvalidPriorityValue(invalid)

    invalid = filter(lambda x: True if x > 5 or x < 1 else False, prioritize.values())

    if len(invalid) > 0:
        raise OutOfRangePriorityValue(invalid)

    return True


def _validate_exclusions(exclusions):
    if len(exclusions) > 0 and exclusions not in TRANSPORT_MODES:
        invalid = filter(lambda x: True if x not in TRANSPORT_MODES else False, exclusions)
        if len(invalid) > 0:
            raise UnknownTransportMode(invalid)

    return True


def _validate_restrictions(restrictions):
    invalid = filter(lambda x: True if x not in RESTRICTION_KEYS else False, restrictions.keys())

    if len(invalid) > 0:
        raise UnknownRestrictionKey(invalid)

    for restriction in restrictions.values():
        invalid = filter(lambda x: True if x not in RESTRICTION_KEYS else False, restriction.keys())

        if len(invalid) > 0:
            raise UnknownRestrictionKey(invalid)

    if 'distance' in restrictions.keys():

        invalid = filter(lambda x: not _check_float(x), restrictions['distance'].values())

        if len(invalid) > 0:
            raise InvalidRestrictionValue(invalid)

    if 'time' in restrictions.keys():

        invalid = filter(lambda x: not _check_integer(x), restrictions['time'].values())

        if len(invalid) > 0:
            raise InvalidRestrictionValue(invalid)

    return True


# </editor-fold>

# <editor-fold desc="INTERNAL">

# Fetch journeys from google API
def _get_journeys(start, end, begin, options=None):
    directions_client = client.Client(DIRECTIONS_API_KEY)
    try:
        begin = parser.parse(begin)
    except Exception:
        raise InvalidDateTimeValue(begin)
    journeys = []
    for mode in TRANSPORT_MODES:
        if not options or \
                'excluded_transport_modes' not in options.keys() or \
                mode not in options["excluded_transport_modes"]:
            journeys.extend(_get_journeys_for_mode(directions_client, start, end, begin, options=options, mode=mode))
    if len(journeys) == 0:
        raise NoRoutesFound()
    return journeys


def _get_journeys_for_mode(directions_client, start, end, begin=datetime.now(), options=None, mode='driving'):
    formatted_journeys = []
    journeys = []
    try:
        journeys = directions.directions(
            directions_client,
            start,
            end,
            departure_time=begin,
            mode=mode,
            alternatives=True
        )
    except Exception as e:
        print(e.args)

    for journey in journeys:
        driving_distance, driving_time = _get_mode_totals(journey, 'driving')
        walking_distance, walking_time = _get_mode_totals(journey, 'walking')
        if options and not _apply_restrictions(driving_distance, driving_time, walking_distance, walking_time, options):
            journeys.pop(journeys.index(journey))
        else:
            time_in_traffic = 0
            if mode in 'driving':
                time_in_traffic = sum(map(lambda x: x['duration_in_traffic']['value'], journey['legs']))
            formatted_journeys.append(
                {
                    "mode": mode,
                    "driving_distance": driving_distance,
                    "driving_time": driving_time,
                    "walking_distance": walking_distance,
                    "walking_time": walking_time,
                    "journey": journey,
                    "time_in_traffic": time_in_traffic / float(
                        sum(map(lambda x: x['duration']['value'], journey['legs']))),
                    "duration": sum(map(lambda x: x['duration']['value'], journey['legs'])),
                    "distance": sum(map(lambda x: x['distance']['value'], journey['legs'])),
                }
            )

    return formatted_journeys


# Apply restriction - returns true if the journey meets restriction otherwise false
def _apply_restrictions(driving_distance, driving_time, walking_distance, walking_time, options):
    if 'restrictions' in options.keys():
        restrictions = options['restrictions']
        if 'distance' in restrictions.keys():
            distance_restrictions = restrictions['distance']
            if 'minimum_drive' in distance_restrictions.keys():
                if driving_distance > 0 and distance_restrictions['minimum_drive'] >= driving_distance / 1000.0:
                    return False
            if 'maximum_walk' in distance_restrictions.keys():
                if distance_restrictions['maximum_walk'] <= walking_distance / 1000.0:
                    return False
        if 'time' in restrictions.keys():
            time_restrictions = restrictions['time']
            if 'minimum_drive' in time_restrictions.keys():
                if driving_time > 0 and time_restrictions['minimum_drive'] >= driving_time / 60.0:
                    return False
            if 'maximum_walk' in time_restrictions.keys():
                if time_restrictions['maximum_walk'] <= walking_time / 60.0:
                    return False

    return True


""" 
Rank journeys by giving each journey a rank between 1 and the length of journeys for each property - 

- least_walking
- shortest
- green_friendly
- least_traffic
- quickest

"""


def _rank_journeys(journeys):
    new_journeys = []
    modified_journeys = copy.deepcopy(journeys)
    for journey in journeys:
        modified_journeys.sort(lambda x, y: -1 if x["walking_distance"] > y["walking_distance"] else 1)
        ranks = {
            "least_walking": modified_journeys.index(journey)
        }
        modified_journeys.sort(lambda x, y: -1 if x["distance"] > y["distance"] else 1)
        ranks["shortest"] = modified_journeys.index(journey)

        modified_journeys.sort(lambda x, y: -1 if x["duration"] > y["duration"] else 1)
        ranks["quickest"] = modified_journeys.index(journey)

        modified_journeys.sort(
            lambda x, y: -1 if TRANSPORT_MODES.index(x['mode']) > TRANSPORT_MODES.index(y['mode']) else 1
        )
        ranks["green_friendly"] = modified_journeys.index(journey)

        modified_journeys.sort(
            lambda x, y: -1 if x['time_in_traffic'] > x['time_in_traffic'] else 1
        )
        ranks["least_traffic"] = modified_journeys.index(journey)

        journey["ranks"] = ranks
        new_journeys.append(journey)

    return new_journeys


# Modify ranks according to priority list
def _prioritize(journeys, options):
    modified_journeys = copy.deepcopy(journeys)
    if options and 'prioritize' in options.keys():
        prioritize = options['prioritize']
        for priority in prioritize.keys():
            for journey in modified_journeys:
                journey['ranks'][priority] *= prioritize[priority]
    return modified_journeys


# Calculate total ranking for each journey - more is better
def _calculate_total(journeys):
    modified_journeys = copy.deepcopy(journeys)
    for journey in modified_journeys:
        journey['total_ranking'] = sum(journey['ranks'].values())
    return modified_journeys


# </editor-fold>

# <editor-fold desc="EXCEPTIONS">


class UnknownPriorityKey(Exception):

    def __init__(self, invalid_keys):
        self.message = "Found unknown priority key(s): %s" % ', '.join(invalid_keys)


class InvalidPriorityValue(Exception):

    def __init__(self, invalid_keys):
        self.message = "Found invalid priority value(s): %s" % ', '.join(map(lambda x: str(x), invalid_keys))


class OutOfRangePriorityValue(Exception):

    def __init__(self, invalid_keys):
        self.message = "Priority values must be an integer between 1 and 5, found: %s" % \
                       ', '.join(map(lambda x: str(x), invalid_keys))


class UnknownTransportMode(Exception):

    def __init__(self, invalid_keys):
        self.message = "Found unknown transport mode(s): %s" % ', '.join(invalid_keys)


class UnknownRestrictionKey(Exception):

    def __init__(self, invalid_keys):
        self.message = "Found unknown restriction key(s): %s" % ', '.join(invalid_keys)


class InvalidRestrictionValue(Exception):

    def __init__(self, invalid_keys):
        self.message = "Found invalid restriction value(s): %s" % ', '.join(map(lambda x: str(x), invalid_keys))


class NoRoutesFound(Exception):

    def __init__(self):
        super(NoRoutesFound, self).__init__("No routes found")


class InvalidDateTimeValue(Exception):

    def __init__(self, value):
        self.message = "Found invalid datetime value: %s" % value



# </editor-fold>


# <editor-fold desc="UTILS">
def _check_integer(x):
    try:
        int(x)
    except ValueError:
        return False
    return True


def _check_float(x):
    try:
        float(x)
    except ValueError:
        return False
    return True


def _get_mode_totals(journey, mode):
    distance = 0
    duration = 0
    for leg in journey['legs']:
        distance += sum(
            map(
                lambda x: x['distance']['value'],
                filter(lambda x: x['travel_mode'] in mode.upper(),
                       leg['steps']
                       )
            )
        )

        duration += sum(
            map(
                lambda x: x['duration']['value'],
                filter(lambda x: x['travel_mode'] in mode.upper(),
                       leg['steps']
                       )
            )
        )
    return distance, duration


# </editor-fold>


if __name__ == '__main__':
    app.run()
